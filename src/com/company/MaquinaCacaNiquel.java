package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MaquinaCacaNiquel {

    private static int fichas = 1;
    static List<Simbolos> valoresSorteados = new ArrayList<>();

    public static int getFichas() {
        return fichas;
    }

    public static void setFichas(int fichas_parametro) {
        fichas = fichas_parametro;
    }

    public static void limpaListaSorteada() {
        valoresSorteados.clear();
    }

    public static void sorteiaValores(){
        limpaListaSorteada();
        for (int i = 1; i <= fichas; i++ ) {
            for (int j = 0; j < 3; j++) {
                Random random = new Random();
                valoresSorteados.add(Simbolos.values()[random.nextInt(Simbolos.values().length)]);
            }
        }
    }

    public static void exibeResultadoSorteado() {
        for (Simbolos resultado: valoresSorteados) {
            IO.exibeMensagem(StringFixas.VALOR_SORTEADO + resultado + StringFixas.PONTOS + resultado.getPontuacao());
        }
    }

    public static void exibePontuacaoSorteio() {
        boolean isMultiplica = true;
        int resultadoPontos = 0;
        String valorSorteado = "";
        for (int i = 0; i < valoresSorteados.size(); i++) {

            if (resultadoPontos == 0) {
                valorSorteado =  String.valueOf(valoresSorteados.get(0));
            }
            if (!valorSorteado.equals(String.valueOf(valoresSorteados.get(i)))) isMultiplica = false;
            resultadoPontos += valoresSorteados.get(i).getPontuacao();

        }

        if (isMultiplica) {
            resultadoPontos =  (resultadoPontos * 100);
        }

        IO.exibeMensagem(StringFixas.SUA_PONTUACAO + String.valueOf(resultadoPontos));
    }
}
