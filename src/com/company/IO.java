package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {

    public static void exibeMensagem(String mensagem) {
        System.out.println(mensagem);
    }

    public void montaMenu() {

        limpaConsole();
        exibeMensagem(StringFixas.O_QUE_FAZER);
        for (int i = 0; i < OpcoesMenu.values().length; i++) {
            exibeMensagem(String.valueOf(OpcoesMenu.values()[i]));
        }

        identificarMenuDesejado();
    }

    public void limpaConsole() {
    }

    public void jogar() {
        //efetuaLeituraDeFichas();
        exibeMensagem(StringFixas.INICIANDO_JOGO);
        exibeMensagem(StringFixas.SORTEANDO_VALORES);
        MaquinaCacaNiquel.sorteiaValores();
        exibeMensagem(StringFixas.RESULTADO);
        MaquinaCacaNiquel.exibeResultadoSorteado();
        MaquinaCacaNiquel.exibePontuacaoSorteio();

        montaMenu();
    }

    public void identificarMenuDesejado() {
        String opcaoDesejada = lerDadosUsuario();
        if (opcaoDesejada.equals("1")) {
            jogar();
        } else if (opcaoDesejada.equals("2")) {
            sair();
        }
    }

    public void sair() {
        System.exit(0);
    }

    public void efetuaLeituraDeFichas() {
        exibeMensagem(StringFixas.QUESTIONA_QUANTIDADE_DE_FICHAS);
        try {
            MaquinaCacaNiquel.setFichas(Integer.parseInt(lerDadosUsuario()));
        } catch (Exception e) {
            exibeMensagem(StringFixas.VALOR_NAO_IDENTIFICADO);
            System.exit(0);
        }
    }

    public static String lerDadosUsuario() {
        try {
            Scanner scanner = new Scanner(System.in);
            return scanner.next();
        } catch (Exception e) {
            IO.exibeMensagem(StringFixas.VALOR_NAO_IDENTIFICADO);
        }
        return "";
    }

    public static boolean isContinuaJogando(String mensagem) {
        IO.exibeMensagem(mensagem);
        try {
            Scanner scanner = new Scanner(System.in);
            String valor = scanner.next();
            if (valor.equals("1")) {
                return true;
            } else if (valor.equals("2")) {
                return false;
            } else {
                IO.exibeMensagem(StringFixas.VALOR_NAO_IDENTIFICADO);
                return false;
            }
        } catch (Exception e) {
            IO.exibeMensagem(StringFixas.VALOR_NAO_IDENTIFICADO);
            System.exit(0);
        }
        return false;
    }


}
