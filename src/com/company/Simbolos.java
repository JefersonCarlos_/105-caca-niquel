package com.company;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

import java.util.HashMap;
import java.util.List;

public enum Simbolos {

    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    PAUS(20000000),
    SETE(300);

    private int pontuacao;

    Simbolos(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public int getPontuacao() {
        return pontuacao;
    }
}
