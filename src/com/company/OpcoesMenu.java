package com.company;

public enum OpcoesMenu {

    JOGAR("1 - Jogar"),
    SAIR("2 - Sair");

    private String valorMenu = "";

    OpcoesMenu(String valorMenu_) {
        this.valorMenu = valorMenu_;
    }

    public String getValorMenu() {
        return valorMenu;
    }

    public void setValorMenu(String valorMenu) {
        this.valorMenu = valorMenu;
    }

    @Override
    public String toString() {
        return  valorMenu;
    }
}
